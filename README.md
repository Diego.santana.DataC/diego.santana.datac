# Este es un ejemplo para titulo donde usaremos el simbolo (#) para ver cada una de las 5 opciones disponibles 


* Con el simbolo de (*) haremos listas en desorden
    * Junto con el Tabulador y el sibolo podremos hacer un espacio
        * Nuevamenta realizamos una tablacion y tendremos en cuenta que lossimbolos para este tipo de listas son __(*,-,+)__

1. Con los números podemos hace tambien listas pero en este caso de manera ordenada
    1. Acá ya podemos ver la tabulacion
        1. Tengamos en cuenta que tambien podremos poner algun fragmento de texto con negrita, cursiva y tachado, para estos casos usaremos __(El doble guion bajo para la negrita)__, ~~(El doble simbolo de ~ para que el texto nos quede tachado)~~ y por ultimo el uso de *(Un solo * nos genera la letra cursiva)*

----

# Acá podemos integrar Link 

### Ejemplo

La empresa [__TESLA__](https://www.tesla.com/ "Pagína oficial Tesla") tiene muchas ambiciones a futuro para poder aportar al planeta con energías renovables. **(Para este Ejemplo usamos la sintaxis de *[Encerrar la palabra contenedora del link de esta manera]*, seguido de encerrar el link dentro de parentesis (https://...) )** 

<diegosantana0520@gmail.com> **Usamos el simbolo <> para poner correos electronicos o Links segun sea el caso**

Esto es una referencia [Tesla]

[Tesla]: https://www.tesla.com/

----

# Añadiendo imagenes 

![Representacion de tesla](https://cloudfront-us-east-1.images.arcpublishing.com/infobae/WKY46NZE6FEFFNUZLPG2VIPPEE.jpg "Tesla")
![Badges](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white) ![GitLab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)


---

# Tablas

| Compañia | Modelo | Precio |
|----------|--------|--------|
| Porsche  | Cayman turbo | 500.000.000 |
| Renault  | Logan  | 21.000.000 |

Para generar tablas de manera rapida y facil entar al siguiente [Link](https://www.tablesgenerator.com/markdown_tables)

---

# Combinación de Markdown y HTML

<img src="https://cloudfront-us-east-1.images.arcpublishing.com/infobae/WKY46NZE6FEFFNUZLPG2VIPPEE.jpg" width= 200px height= 150px> __Esto en el caso de que si quiera cambiar el tamaño de una imagen o hacer cosas que no te permite *MD*__

---

# Simulación de un video

Cuando queremos hacer la simulación de algun video debemos usar la siguiente sintaxis __[![Informacion a adicionar](https://img.youtube.com/vi/el id del video/maxresdefault.jpg)](Link del video)__

[![Tesla Model X](https://img.youtube.com/vi/yHdd_okRe6Q/maxresdefault.jpg)](https://www.youtube.com/watch?v=yHdd_okRe6Q&t=30s)

---
__[![Informacion a adicionar](https://img.youtube.com/vi/el id del video/hqdefault.jpg)](Link del video)__

[![Tesla Model X](https://img.youtube.com/vi/yHdd_okRe6Q/hqdefault.jpg)](https://www.youtube.com/watch?v=yHdd_okRe6Q&t=30s)

---

__[![Informacion a adicionar](https://img.youtube.com/vi/el id del video/default.jpg)](Link del video)__

[![Tesla Model X](https://img.youtube.com/vi/yHdd_okRe6Q/default.jpg)](https://www.youtube.com/watch?v=yHdd_okRe6Q&t=30s)

 ---
 ---

 # Hacks de texto
```html
Con la etiqueta <ins> e </ins>
```
<ins>podremos realizar textos subrayados</ins>

```html
Tambien podemos alinear texto al centro con la etiqueta <center> y </center>
```

<center><ins>Y asi veremos el texto con su alineacion al centro</ins></center>

```html
Tambien podemos cambiar de color los textos con la etiqueta <p style="color:color en ingles"> y </p>
```
<p style="color:red">Color Rojo</p>

```html
Por ultimo podemos hacer que los links se abran en una nueva pestaña con la etiquera <a href="link de la pagina" target="_blank">texto deseado</a>
```

* <a href="https://www.tesla.com/" target="_blank">Pagina oficial de __Tesla__</a>

---

# Bloques de codigo

```javascript
let my_variable= "Hola mundo";
const console = () => {
    console.log(my_variable);
};
```
> Esto es una cita en MarkDown

# Referencias

Here is a simple footnote[^1].

A footnote can also have multiple lines[^2].  

You can also use words, to fit your writing style more closely[^note].

[^1]: My reference.
[^2]: Every new line should be prefixed with 2 spaces.  
  This allows you to have a footnote with multiple lines.
[^note]:
    Named footnotes will still render with numbers instead of the text but allow easier identification and linking.  
    This footnote also has been made with a different syntax using 4 spaces for new lines.

    
